import {VitrailPlugin} from "../../core/index";
import {SourceMapGenerator} from "source-map";
import {readFileSync} from "fs";
import {dirname, relative, resolve} from "path";
import {Rebaser} from "css-source-map-rebase";

export const createCssRebaserPlugin = (): VitrailPlugin => {
    return (resource, context) => {
        const {schedule} = context;

        const rebaser = new Rebaser({
            rebase: (source, resolvedPath, done) => {
                const sourceDirectory = dirname(source);
                const resolvedPathRelativeToSourceDirectory = relative(sourceDirectory, resolvedPath);
                const resourcePath = resolve(sourceDirectory, resolvedPathRelativeToSourceDirectory);

                done(schedule(resourcePath));
            }
        });

        let {data, dependencies, name, sourceMap} = resource;

        if (data === undefined) {
            data = readFileSync(name);
        }

        if (sourceMap === undefined) {
            const sourceMapGenerator = new SourceMapGenerator();

            sourceMapGenerator.addMapping({
                generated: {
                    column: 0,
                    line: 1
                },
                original: {
                    column: 0,
                    line: 1
                },
                source: name
            });

            sourceMap = sourceMapGenerator.toJSON();
        }

        return rebaser.rebase(
            data,
            sourceMap
        ).then((result) => {
            const {css, map} = result;

            return {
                dependencies,
                data: css,
                name,
                sourceMap: map
            };
        });
    };
};
