import {VitrailPlugin} from "../../core/index";
import {SourceMapGenerator} from "source-map";
import {readFileSync} from "fs";
import {dirname, relative, resolve} from "path";
import {createRebaser} from "html-source-map-rebase";

export const createHtmlRebaserPlugin = (): VitrailPlugin => {
    return (resource, context) => {
        const {schedule} = context;

        const rebaser = createRebaser({
            rebase: (source, resolvedPath, done) => {
                const sourceDirectory = dirname(source);
                const resolvedPathRelativeToSourceDirectory = relative(sourceDirectory, resolvedPath);
                const resourcePath = resolve(sourceDirectory, resolvedPathRelativeToSourceDirectory);

                done(schedule(resourcePath));
            }
        });

        let {data, dependencies, name, sourceMap} = resource;

        if (data === undefined) {
            data = readFileSync(name);
        }

        if (sourceMap === undefined) {
            const sourceMapGenerator = new SourceMapGenerator();

            sourceMapGenerator.addMapping({
                generated: {
                    column: 0,
                    line: 1
                },
                original: {
                    column: 0,
                    line: 1
                },
                source: name
            });

            sourceMap = sourceMapGenerator.toJSON();
        }

        return rebaser.rebase(
            data,
            sourceMap
        ).then((value) => {
            const {data, map} = value;

            return {
                dependencies,
                data,
                name,
                sourceMap: map
            };
        });
    };
};
