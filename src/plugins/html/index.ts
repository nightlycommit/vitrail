import type {VitrailPlugin} from "../../core/index";
import {readFileSync} from "fs";
import {SourceMapGenerator} from "source-map";
import {createHtmlRebaserPlugin} from "../html-rebaser/index";

export const createHtmlPlugin = (): VitrailPlugin => {
    const rebaserPlugin = createHtmlRebaserPlugin();

    return (resource, context) => {
        let {data, name} = resource;

        if (data === undefined) {
            data = readFileSync(name);
        }

        const sourceMapGenerator = new SourceMapGenerator();

        sourceMapGenerator.addMapping({
            generated: {
                column: 0,
                line: 1
            },
            original: {
                column: 0,
                line: 1
            },
            source: name
        });

        const sourceMap = sourceMapGenerator.toJSON();

        return rebaserPlugin({
            dependencies: [
                name
            ],
            data,
            name,
            sourceMap
        }, context);
    };
};
