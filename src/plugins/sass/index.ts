import type {Exception, Options, CompileResult} from "sass";
import {compile, compileString} from "sass";
import {dirname} from "path";
import type {VitrailPlugin, VitrailResource} from "../../core/index";
import {createCssRebaserPlugin} from "../css-rebaser/index";

export const createSassPlugin = (
    options?: Options<"sync">
): VitrailPlugin => {
    const rebaserPlugin = createCssRebaserPlugin();

    return (resource, context) => {
        const {emitError} = context;
        const {name, data} = resource;

        try {
            let result: CompileResult;

            const actualOptions = options || {};

            actualOptions.sourceMap = true;
            actualOptions.sourceMapIncludeSources = true;

            if (data) {
                actualOptions.loadPaths = actualOptions.loadPaths || [];
                actualOptions.loadPaths.push(dirname(name));

                result = compileString(data.toString(), actualOptions);
            } else {
                result = compile(name, actualOptions);
            }

            const {sourceMap, loadedUrls} = result;
            const dependencies = loadedUrls.map((loadedUrl) => loadedUrl.pathname);

            resource = {
                dependencies,
                data: Buffer.from(result.css),
                name,
                sourceMap: sourceMap ? {
                    file: sourceMap.file || '',
                    version: Number.parseInt(sourceMap.version),
                    sourcesContent: sourceMap.sourcesContent,
                    sources: sourceMap.sources,
                    names: sourceMap.names,
                    mappings: sourceMap.mappings,
                    sourceRoot: sourceMap.sourceRoot
                } : undefined
            };
        } catch (error: any) {
            // console.error(exception);
            //
            // if (exception.span.url) {
            //     outputContext.resources.push({
            //         name: exception.span.url.pathname,
            //         dependencies: [
            //             exception.span.url.pathname
            //         ]
            //     });
            // }
            //
            // outputContext.error = error;

            emitError(error as Exception);
        }

        return rebaserPlugin(resource, context);
    }
};
