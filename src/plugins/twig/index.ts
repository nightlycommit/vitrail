import {createSourceMapRuntime, TwingEnvironment} from "twing";
import type {VitrailPlugin} from "../../core/index";
import {createHtmlRebaserPlugin} from "../html-rebaser/index";

export const createTwigPlugin = (
    environmentFactory: () => Promise<TwingEnvironment>
): VitrailPlugin => {
    const rebaserPlugin = createHtmlRebaserPlugin();

    return (resource, context) => {
        return environmentFactory()
            .then((environment) => {
                const {name, dependencies} = resource;
                const loadedTemplates: Array<[name: string, from: string | null]> = [];

                dependencies.push(name);

                environment.on("load", (name, from) => {
                    loadedTemplates.push([name, from]);
                });

                const sourceMapRuntime = createSourceMapRuntime()

                return environment.loadTemplate(name)
                    .then((template) => {
                        return template.render(resource, {
                            sourceMapRuntime
                        });
                    })
                    .then((output) => {
                        const {sourceMap} = sourceMapRuntime;

                        return Promise.all(loadedTemplates.map(([name, from]) => {
                            return environment.loader.resolve(name, from || null)
                                .then((resolvedTemplatePath) => {
                                    return resolvedTemplatePath || name;
                                });
                        })).then((resolvedTemplates) => {
                            dependencies.push(...resolvedTemplates);

                            const data = Buffer.from(output);

                            return rebaserPlugin({
                                dependencies,
                                data,
                                name,
                                sourceMap: {
                                    file: sourceMap.file || '',
                                    version: Number.parseInt(sourceMap.version),
                                    sources: sourceMap.sources,
                                    names: sourceMap.names,
                                    mappings: sourceMap.mappings,
                                    sourceRoot: sourceMap.sourceRoot,
                                    sourcesContent: sourceMap.sourcesContent
                                }
                            }, context);
                        });
                    });
            });
    };
};
