import createVirtualPlugin from "@rollup/plugin-virtual";
import {
    createTypeScriptPlugin as createRollupTypeScriptPlugin,
    FactoryOptions
} from "@vitrail/rollup-plugin-typescript";
import createNodeResolvePlugin from "@rollup/plugin-node-resolve";
import {dirname, relative} from "path";
import type {Plugin, OutputOptions} from "rollup";
// @ts-ignore
import createIncludePathsPlugin from "rollup-plugin-includepaths";  // todo: replace with something serious
import type {VitrailResource, VitrailPlugin} from "../../core/index";
import {rollup} from "rollup";

export const createTypeScriptPlugin = (
    options: FactoryOptions & {
        additionalPlugins?: Array<Plugin>;
    },
    outputOptions: Pick<OutputOptions, "format">
): VitrailPlugin => {
    const nodeResolvePlugin = createNodeResolvePlugin();
    const typeScriptPlugin = createRollupTypeScriptPlugin(options);

    return (resource, context) => {
        const resourceHandlers: Array<Promise<void>> = [];
        const results: Array<VitrailResource> = [];

        const {name, data} = resource;

        const plugins: Array<Plugin> = [
            nodeResolvePlugin,
            typeScriptPlugin
        ];

        if (options?.additionalPlugins) {
            plugins.push(...options.additionalPlugins);
        }

        if (data) {
            plugins.unshift(
                createIncludePathsPlugin({
                    paths: [dirname(name)]
                })
            );

            plugins.unshift(
                createVirtualPlugin({
                    [name]: data.toString()
                })
            );
        }

        const actualOutputOptions: OutputOptions = {
            ...outputOptions,
            sourcemap: true,
            sourcemapExcludeSources: false
        };

        return rollup({
            input: name,
            plugins
        }).then((build) => {
            return build.generate(actualOutputOptions);
        }).then((output) => {
            const chunks: VitrailResource["chunks"] = [];
            const dependencies: Array<string> = [];

            let resource: VitrailResource;

            for (const chunkOrAsset of output.output) {
                if (chunkOrAsset.type === "chunk") {
                    const {code, isEntry} = chunkOrAsset;
                    const {file, mappings, names, sources, sourcesContent, version} = chunkOrAsset.map!;

                    if (sources.length > 0) {
                        dependencies.push(...sources);
                    } else {
                        dependencies.push(name);
                    }

                    const data: VitrailResource["data"] = Buffer.from(code);

                    const sourceMap: VitrailResource["sourceMap"] = {
                        file,
                        mappings,
                        names,
                        sources,
                        sourcesContent: sourcesContent?.map((sourceContent) => {
                            return sourceContent === null ? '' : sourceContent
                        }),
                        version
                    };

                    if (isEntry) {
                        resource = {
                            dependencies,
                            data: Buffer.from(code),
                            name,
                            sourceMap,
                            chunks: []
                        }
                    } else {
                        resource!.chunks!.push({
                            data,
                            name: chunkOrAsset.name,
                            sourceMap
                        });
                    }
                }
            }

            return resource!;
        });
    };
};
