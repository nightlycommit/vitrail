import {Command, createArgument, createOption} from "commander";
import {start} from "./cli";

export const main = (argv: readonly string[]) => {
    const program = new Command('vitrail')
        .addArgument(createArgument('workingDirectory').argOptional().default('.'))
        .addOption(createOption('-c, --configuration-path <configurationPath>'))
        .addOption(createOption('-e, --entry <entry>'))
        .addOption(createOption('-o, --output-directory <outputDirectory>'))
        .action((workingDirectory: string, options: {
            configurationPath?: string;
            entry?: string;
            outputDirectory?: string;
        }) => {
            return start(workingDirectory, options).then();
        });

    program.parse(argv);
};
