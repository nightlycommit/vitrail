import {resolve} from "path";
import type {VitrailConfiguration} from "../core";
import {createApplication} from "../core";
import findupSync from "findup-sync";
import {defaultConfiguration} from "../configuration";

const configurationCandidates = [
    'vitrail.configuration.mjs'
];

const resolveConfigurationFromPath = (configurationPath: string | null = null): Promise<VitrailConfiguration | null> => {
    if (configurationPath === null) {
        configurationPath = findupSync(configurationCandidates);
    }

    if (configurationPath === null) {
        return Promise.resolve(null);
    }

    const resolvedConfigurationPath = resolve(configurationPath);

    return import(resolvedConfigurationPath)
        .then((module: { default: VitrailConfiguration }) => module.default);
};

export const start = (workingDirectory: string, options: {
    configurationPath?: string;
    entry?: string;
    outputDirectory?: string;
}) => {
    const {configurationPath, entry} = options;
    
    const resolveConfiguration: Promise<VitrailConfiguration> = resolveConfigurationFromPath(configurationPath)
        .then((configuration) => {
            if (configuration === null) {
                return defaultConfiguration;
            }

            return configuration;
        })

    return resolveConfiguration
        .then((configuration) => {
            if (options.entry) {
                configuration.entry = options.entry;
            }

            if (options.outputDirectory) {
                configuration.outputDirectory = options.outputDirectory;
            }

            const application = createApplication(
                workingDirectory,
                configuration
            );

            return application();
        });
};
