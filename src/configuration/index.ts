import type {VitrailRoute} from "../core";
import {createHtmlPlugin} from "../plugins/html";
import {createTwigPlugin} from "../plugins/twig";
import {createEnvironment, createFilesystemLoader} from "twing";
import fs from "fs";
import {createSassPlugin} from "../plugins/sass";
import {createTypeScriptPlugin} from "../plugins/typescript";
import {createCopyPlugin} from "../plugins/copy";

export const defaultHtmlPattern = /\.html$/;
export const defaultSassPattern = /\.s[ac]ss$/;
export const defaultTwigPattern = /\.twig$/;
export const defaultTypeScriptPattern = /\.ts$/;

export const defaultHtmlRoute: VitrailRoute = {
    pattern: defaultHtmlPattern,
    plugin: createHtmlPlugin(),
    output: (name) => name
};

export const defaultSassRoute: VitrailRoute = {
    pattern: defaultSassPattern,
    plugin: createSassPlugin(),
    output: (name) => {
        return name.replace(defaultSassPattern, '.css');
    }
};

export const defaultTwigRoute: VitrailRoute = {
    pattern: defaultTwigPattern,
    plugin: createTwigPlugin(
        () => Promise.resolve(createEnvironment(
            createFilesystemLoader(fs)
        ))
    ),
    output: (name) => {
        return name.replace(defaultTwigPattern, '');
    }
};

export const defaultTypeScriptRoute: VitrailRoute = {
    pattern: defaultTypeScriptPattern,
    plugin: createTypeScriptPlugin({
        include: defaultTypeScriptPattern
    }, {
        format: "module"
    }),
    output: (name) => {
        return name.replace(defaultTypeScriptPattern, '.mjs');
    }
};

export const defaultFallbackRoute: VitrailRoute = {
    pattern: /.*/,
    plugin: createCopyPlugin(),
    output: (name) => name
};

const routes: Array<VitrailRoute> = [
    defaultHtmlRoute,
    defaultSassRoute,
    defaultTwigRoute,
    defaultTypeScriptRoute,
    defaultFallbackRoute
];

export const defaultConfiguration = {
    entry: 'index.html.twig',
    outputDirectory: 'www',
    routes
};
