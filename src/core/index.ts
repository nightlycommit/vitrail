export type {VitrailApplication} from "./lib/application";
export type {VitrailConfiguration} from "./lib/configuration";
export type {VitrailContext, VitrailResource} from "./lib/context";
export type {VitrailPlugin} from "./lib/plugin";
export type {VitrailRoute} from "./lib/route";

export {createApplication} from "./lib/application";
