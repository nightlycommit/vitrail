import type {VitrailPlugin} from "./plugin";

export interface VitrailRoute {
    pattern: RegExp;
    plugin: VitrailPlugin;
    output: (name: string) => string;
}

export const getMatchingRoute = (name: string, routes: Array<VitrailRoute>): VitrailRoute | null => {
    return routes.find((route) => {
        return route.pattern.test(name);
    }) || null;
};
