import {outputFileSync, emptyDirSync} from "fs-extra";
import {relative, join, resolve, dirname, basename} from "path";
import {watch} from "chokidar";
import BrowserSync, {type BrowserSyncInstance} from "browser-sync";
import {type VitrailConfiguration} from "./configuration";
import {type VitrailContext, type VitrailResource} from "./context";
import {getMatchingRoute, type VitrailRoute} from "./route";
import chalk from "chalk";

type Stop = () => Promise<void>;

export type VitrailApplication = () => Promise<Stop>;

type Task = {
    route: VitrailRoute;
    source: string;
    target: string;
};

export const createApplication = (
    workingDirectory: string,
    configuration: VitrailConfiguration
): VitrailApplication => {
    const {routes} = configuration;
    const relativeWorkingDirectory = relative('.', workingDirectory);
    const outputDirectory = join(configuration.outputDirectory, relativeWorkingDirectory);
    const entryPoint = resolve(`${workingDirectory}/${configuration.entry}`);

    const resolvePath = (path: string) => join('<root>', resolve(path));

    const createTask = (name: string): Task => {
        const route = getMatchingRoute(name, routes);

        if (!route) {
            throw new Error(`No route found for "${name}"`);
        }

        let target = route.output(name);

        if (name === entryPoint) {
            target = basename(target);
        } else {
            target = resolvePath(target);
        }

        return {
            route,
            source: name,
            target
        };
    };

    const handle = async (name: string, browserSyncInstance: BrowserSyncInstance): Promise<void> => {
        const urls = browserSyncInstance.getOption("urls");
        const start = new Date();

        console.info(`[${chalk.blue(start.toLocaleString())}] [${urls.get("local")}] Building ${chalk.bgRed(name)}...`);

        const scheduledTasks: Array<Task> = [];
        const executedTasks: Array<Task> = [];

        const schedule = (name: string, from: Task | null): string => {
            let task = [...executedTasks, ...scheduledTasks].find((task) => {
                return task.source === name;
            });

            if (!task) {
                task = createTask(name);

                scheduledTasks.push(task);
            }

            let target = task.target;

            if (from) {
                target = relative(dirname(from.target), target);
            }

            return target;
        };

        const handleTask = (task: Task): Promise<VitrailResource | null> => {
            const {source, route} = task;

            const emittedErrors: Array<Error> = [];

            const context: VitrailContext = {
                emitError: (error: Error): void => {
                    emittedErrors.push(error);
                },
                schedule: (resource: string) => schedule(resource, task)
            };

            return route.plugin({
                name: source,
                dependencies: []
            }, context)
                .then((resource) => {
                    // normalize dependencies
                    resource.dependencies = resource.dependencies?.map((dependency) => resolve(dependency)) || [];

                    const {dependencies} = resource;

                    const watchFiles = () => {
                        const watcher = watch(dependencies);

                        watcher.on("change", () => {
                            // one of the dependency of the resource did change: rebuild the resource
                            return watcher.close()
                                .then(() => {
                                    return handle(resource.name, browserSyncInstance);
                                });
                        });
                    };

                    watchFiles();

                    return resource;
                });
        };

        schedule(name, null);

        const resources: Array<VitrailResource & Task> = [];

        while (scheduledTasks.length > 0) {
            const stackEntry = scheduledTasks.pop()!;
            const stackEntryResource = await handleTask(stackEntry);

            if (stackEntryResource) {
                resources.push({
                    ...stackEntryResource,
                    ...stackEntry
                });
            }

            executedTasks.push(stackEntry);
        }

        for (const resource of resources) {
            if (resource.data) {
                let fileName = resource.target;

                outputFileSync(join(outputDirectory, fileName), resource.data);
            }
        }

        const end = new Date();
        const duration = `${end.getTime() - start.getTime()}ms`;

        console.info(`[${chalk.blue(end.toLocaleString())}] [${urls.get("local")}] ${chalk.bgRed(name)} built in ${chalk.bgMagenta(duration)}`);
        console.info(`[${chalk.blue(end.toLocaleString())}] [${urls.get("local")}] ${chalk.dim('Waiting for changes 👀')}`);
    };

    return () => {
        emptyDirSync(outputDirectory);

        const browserSyncInstance = BrowserSync.create();

        return new Promise((resolve, reject) => {
            const subPath = relative('.', workingDirectory);

            browserSyncInstance.init({
                server: join(outputDirectory, subPath),
                open: false,
                watch: true,
                logLevel: "silent"
            }, (error, instance) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(handle(entryPoint, instance)
                        .then(() => {
                            return () => {
                                browserSyncInstance.exit();

                                return Promise.resolve();
                            }
                        })
                    );
                }
            });
        });
    };
};
