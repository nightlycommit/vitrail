import type {VitrailRoute} from "./route";

export type VitrailConfiguration = {
    entry: string;
    outputDirectory: string;
    routes: Array<VitrailRoute>;
};