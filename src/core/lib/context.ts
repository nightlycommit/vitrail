import type {RawSourceMap} from "source-map";
import type {VitrailRoute} from "./route";

export type VitrailResource = {
    data?: Buffer;
    dependencies: Array<string>;
    name: string;
    sourceMap?: RawSourceMap;
    chunks?: Array<{
        data: Buffer;
        name: string;
        sourceMap?: RawSourceMap;
    }>;
};

export interface VitrailContext {
    emitError(error: Error): void;

    schedule(name: string): string;
}
