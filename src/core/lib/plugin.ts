import {type VitrailContext, type VitrailResource} from "./context";

export type VitrailPlugin = (
    resource: VitrailResource,
    context: VitrailContext
) => Promise<VitrailResource>;
