import {createCommand, createArgument, createOption} from "commander";
import typeScript from "typescript";
import {join} from "path";
import {readFileSync} from "fs";
import {emptyDirSync} from "fs-extra";

const build = (
    destination
) => {
    const configFilePath = typeScript.findConfigFile(process.cwd(), typeScript.sys.fileExists);
    
    const tsConfigFile = configFilePath ? typeScript.readConfigFile(configFilePath, typeScript.sys.readFile).config : {};

    const parsedCommandLine = typeScript.parseJsonConfigFileContent(
        tsConfigFile,
        typeScript.sys,
        process.cwd()
    );

    const options = parsedCommandLine.options;

    options.outDir = destination;
    options.declaration = true;
    
    const program = typeScript.createProgram({
        options,
        rootNames: [
            'src/index.ts',
            'src/main.ts'
        ]
    });

    const optionsDiagnostics = program.getOptionsDiagnostics();

    if (optionsDiagnostics.length > 0) {
        console.error(optionsDiagnostics);

        return;
    }

    const semanticDiagnostics = program.getSemanticDiagnostics();

    if (semanticDiagnostics.length > 0) {
        console.error(semanticDiagnostics);

        return;
    }

    program.emit(undefined, (fileName, data) => {
        if (fileName.endsWith('main.js') || fileName.endsWith('main.d.ts')) {
            if (fileName.endsWith('main.js')) {
                typeScript.sys.writeFile(fileName, `#!/usr/bin/env node
${data.replace('exports.main = main;', 'main(process.argv);')}`);
            }
        }
        else {
            typeScript.sys.writeFile(fileName, data);
        }
    });
}

const writeManifest = (
    destination,
    version
) => {
    const manifest = JSON.parse(readFileSync('./package.json').toString());

    delete manifest['devDependencies'];
    delete manifest['scripts'];

    manifest['version'] = version;
    
    manifest['bin'] = {
        'vitrail': './main.js'
    };

    manifest['main'] = `./index.js`;
    manifest['types'] = `./index.d.ts`;

    typeScript.sys.writeFile(join(destination, 'package.json'), JSON.stringify(manifest));
};

const program = createCommand('node build.mjs')
    .addArgument(createArgument('destination'))
    .addOption(createOption('--version <version>').default('0.0.0'))
    .addOption(createOption('--source-map').default(false))
    .action((destination, options) => {
        emptyDirSync(destination);
        build(destination);
        writeManifest(destination, options.version);
        
        return Promise.resolve();
    });

program.parse(process.argv);
